FROM ruby:2.6

RUN bundle config --global frozen 1
RUN bundle config set deployment 'true'

WORKDIR /opt/todos/

COPY . .
# RUN gem install bundler:2.1.4
RUN bundle version
RUN bundle install --local

# Persistent volumes
VOLUME /opt/todos/data
EXPOSE 4567

CMD ["bundle", "exec", "ruby", "server.rb"]

