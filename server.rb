# server.rb
require 'sinatra'
require 'data_mapper'
require 'json'

# set :public_folder, File.dirname(__FILE__) + '/public'
set :bind, '0.0.0.0'
set :show_exceptions, true
set :raise_errors, true
set :dump_errors, true

dblocation = ENV['TODOS_DB_LOCATION']
dblocation ||= '/opt/todos/data//todo.db'
DataMapper.setup(:default, "sqlite://#{dblocation}")

class Todo
  include DataMapper::Resource
  property :id,         Serial
  property :title,      String
  property :completed,  Boolean
end

DataMapper.auto_upgrade!

get '/' do
  send_file File.join(settings.public_folder, 'index.html')
end

get '/todos' do
  content_type :json
  todos = Todo.all
  logger.info "get #{todos.to_json}"
  return todos.to_json
end

post '/todos' do
  data = request.body.read
  logger.info "post #{data}"
  if data.empty?
    return Todo.all.to_json
  end

  payload = JSON.parse(data)
  logger.info "post #{payload.to_json}"
  Todo.all.destroy
  payload.each do |t|
    logger.info "Create #{t}"
    Todo.create(title: t["title"], completed: t["completed"])
  end
end
